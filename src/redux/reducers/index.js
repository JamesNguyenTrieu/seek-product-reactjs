import { combineReducers } from 'redux';
import productReducer from "./ProductReducer";
import navBarReducer from "./NavBarReducer";
import cartReducer from "./CartReducer";
import categoryReducer from "./CategoryReducer";

const myReducer = combineReducers({
    productReducer,
    categoryReducer,
    cartReducer,
    navBarReducer,
});

export default myReducer;