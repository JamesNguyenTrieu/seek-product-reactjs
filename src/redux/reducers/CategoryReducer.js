import * as types from "./../constants/CategoryTypes";

var initialState = {
    categories: []
}

const categoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.FETCH_ALL_CATEGORIES:
            state.categories = action.categories;
            localStorage.setItem('categories', JSON.stringify(state.categories));
            return {
                categories: [...state.categories]
            };
        default:
            return state;
    }
}

export default categoryReducer;