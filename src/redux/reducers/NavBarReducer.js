import * as types from "./../constants/NavTypes";

// let navBarShowed = true;
// let currentPage = window.location.href;
// currentPage = (currentPage).slice(currentPage.lastIndexOf("/")+1, currentPage.length);
// if (currentPage === 'signup' || currentPage === 'login') {
//     navBarShowed = false;
// }

// let isSignedIn = false;
// if (localStorage.getItem('user')) {
//     isSignedIn = true;
// }

// var initialNavBarState = {
//     isShowed: navBarShowed,
//     isLoggedIn: isSignedIn
// };

var initialNavBarState = {
    isShowed: true,
    isLoggedIn: false
};

const navBarReducer = (state = initialNavBarState, action) => {
    switch (action.type) {
        case types.SHOW_OR_HIDE_NAV_BAR:
            state.isShowed = action.status;
            if (localStorage.getItem('user')) {
                state.isLoggedIn = true;
            }
            else state.isLoggedIn = false;
            return {
                isShowed: state.isShowed,
                isLoggedIn: state.isLoggedIn
            };
        case types.LOGGED_IN_OR_NOT:
            state.isLoggedIn = action.status;
            return {
                ...state,
                isLoggedIn: state.isLoggedIn
            };
        default:
            return state;
    }
}

export default navBarReducer;