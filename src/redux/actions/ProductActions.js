import * as types from "../constants/ProductsTypes";
import { GetProducts, GetCategoryProducts, GetDetailedProduct } from "../../apiCaller/ProductCaller";

export const actFetchProductsRequest = (pageNumber) => {
    return (dispatch) => {
        return GetProducts(pageNumber).then(res => {
            if (pageNumber === undefined || res === undefined || pageNumber < 1) {
                return dispatch(actFetchProducts(null));
            }
            return dispatch(actFetchProducts(res.data));
        });
    }
}

export const actFetchProducts = (results) => {
    return {
        type: types.FETCH_PRODUCTS,
        results
    }
}

export const fetchOneProduct = (productID) => {
    return (dispatch) => {
        GetDetailedProduct(productID).then(res => {            
            if (res) {
                return dispatch({
                    type: types.FETCH_DETAILED_PRODUCT,
                    results: res.data
                });
            }           
        });
    }
}

export const fetchProductsByCategory = (cat, page) => {
    return (dispatch) => {
        GetCategoryProducts(cat, page).then(res => {
            if (res === undefined) {
                return dispatch(actFetchProducts(null));
            }
            return dispatch(actFetchProductsByCategory(res.data));
        });
    }
}

export const actFetchProductsByCategory = (results) => {
    return {
        type: types.FETCH_PRODUCTS_BY_CATEGORY,
        results
    }
}