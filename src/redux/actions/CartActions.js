import * as types from "./../constants/CartTypes";
import { GetCart, AddToCartCaller, RemoveFromCart, DeleteAllCartItems, UpdateCart, CartPayment2 } from "../../apiCaller/CartCaller";
import { GetDetailedProduct } from "../../apiCaller/ProductCaller";
import axios from "axios";
import { API_URL } from "../../apiCaller/config";

export const fetchCart = () => {
    return (dispatch) => {
        return GetCart().then( res => {
            if (res !== undefined) {
                // let products = new Array(res.data.cart_detail.length);

                // res.data.cart_detail.forEach((element, index) => {
                //     setTimeout(() => {
                //         GetDetailedProduct(element.product.id).then( pro => {
                //             let p = {
                //                 id: pro.data.product.id,
                //                 in_stock: pro.data.product.in_stock
                //             }
                //             setTimeout(() => products[index] = p, 300);
                //         });
                //     }, 300);                    
                // });

                // let requests = [];
                // res.data.cart_detail.forEach(element => {                    
                //     let req = axios.get(`${API_URL}/api/products/${element.product.id}/details`);
                //     requests.push(req);
                // });

                // axios.all(requests).then(responseArr => {
                //     dispatch(actFetchCart(res.data, responseArr));
                // });
                dispatch(actFetchCart(res.data));
            }          
        });
    }
}

// export const actFetchCart = (results, products) => {
//     return {
//         type: types.FETCH_CART,
//         results,
//         products
//     }
// }

export const actFetchCart = (results) => {
    return {
        type: types.FETCH_CART,
        results,
    }
}

export const updateCartItemQuantity = (productID, amount) => {
    return (dispatch) => {
        UpdateCart(productID, amount).then(res => {
            if (res) {
                return dispatch({
                    type: types.UPDATE_CART,
                    results: res.data
                });
            }
        });
    }
}

export const deleteCart = () => {
    return (dispatch) => {
        DeleteAllCartItems().then(res => {
            if (res) {
                return dispatch({
                    type: types.DELETE_CART,
                    results: res.data
                });
            }
        });
    }
}

export const deleteOneItem = (productID) => {
    return (dispatch) => {
        return RemoveFromCart(productID).then(res => {
            if (res !== undefined) {
                return dispatch ({
                    type: types.DELETE_ONE_ITEM,
                    results: res.data
                });
            }
        });
    }
}

export const checkout = () => {
    return (dispatch) => {
        return dispatch({
            type: types.CHECK_OUT,
        });
    }
}

export const actAddToCart = (product, amount) => {
    return (dispatch) => {
        AddToCartCaller(product, amount).then(res => {
            if (res) {
                return dispatch({
                    type: types.ADD_TO_CART,
                    results: res.data
                });
            }
        });
    }
}