import * as types from "./../constants/CategoryTypes";
import GetAllCategories from "../../apiCaller/CategoryCaller";

export const fetchAllCategories = () => {
    return (dispatch) => {
        return GetAllCategories().then(res => {
            dispatch({
                type: types.FETCH_ALL_CATEGORIES,
                categories: res.data.results
            })
        });
    }
    
}