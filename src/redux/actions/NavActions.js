import * as types from "../constants/NavTypes";

export const hideOrShowNavBar = (isShowed) => {
    return {
        type: types.SHOW_OR_HIDE_NAV_BAR,
        status: isShowed
    }
}

export const setLoginStatus = (isLoggedIn) => {
    return {
        type: types.LOGGED_IN_OR_NOT,
        status: isLoggedIn
    }
}