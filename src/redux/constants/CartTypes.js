export const FETCH_CART = 'FETCH_CART';
export const ADD_TO_CART = 'ADD_TO_CART';
export const PUSH_NEW_ITEM_INTO_CART = 'PUSH_NEW_ITEM_INTO_CART';
export const UPDATE_CART = 'UPDATE_CART';
export const DELETE_CART = 'DELETE_CART';
export const DELETE_ONE_ITEM = 'DELETE_ONE_ITEM';
export const CHECK_OUT = 'CHECK_OUT';