import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import myReducer from "../reducers/index";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";

const composeEnhancers = composeWithDevTools({
    // options like actionSanitizer, stateSanitizer
});

const store = createStore(myReducer, composeEnhancers(
    applyMiddleware(thunk),
));

export default store;