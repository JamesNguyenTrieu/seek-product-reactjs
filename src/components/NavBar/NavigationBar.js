import React, { Component, Fragment} from 'react';
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import * as productActions from "../../redux/actions/ProductActions";
import * as navBarActions from "./../../redux/actions/NavActions";
import * as cateActions from "./../../redux/actions/CategoryActions";
import * as cartActions from "./../../redux/actions/CartActions";
import "./NavigationBar.scss"
import Swal from 'sweetalert2';

export class NavigationBar extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            isLoggedIn: false,
            see_more: false
        }        
        this.props.fetchAllCategories();
        if (localStorage.getItem('access_token')) {
            this.props.fetchCart();
        }
    }
    
    componentDidMount() {
        this.checkLoginAndURL();
    }

    chooseCat = (e) => {
        this.props.fetchProductsByCategory(e.target.text, 1);
    }

    getAllProducts = () => {
        this.props.hideOrShowNavBar(true);
        this.props.fetchAllProducts(1);
    }

    checkLoginAndURL = () => {
        let navBarShowed = true;
        let currentPage = window.location.href;
        currentPage = (currentPage).slice(currentPage.lastIndexOf("/")+1, currentPage.length);
        if (currentPage === 'signup' || currentPage === 'login') {
            navBarShowed = false;
        }

        let isSignedIn = false;
        if (localStorage.getItem('user')) {
            isSignedIn = true;
        }

        this.props.hideOrShowNavBar(navBarShowed);
        this.props.setLoginStatus(isSignedIn);
    }

    goToLoginOrSignupPage = () => {
        this.props.hideOrShowNavBar(false);
    }

    logOut = () => {
        this.props.checkout();
        localStorage.removeItem("user");
        localStorage.removeItem("access_token");
        localStorage.removeItem("cart");
        localStorage.removeItem("numberOfProducts");

        this.props.setLoginStatus(false);
    }

    componentDidUpdate() {
        let x = document.getElementsByClassName("lbl");
        if (x[0]) {
            if (this.state.see_more) {
                x[0].innerHTML = "See less";
                for (const x of document.getElementsByClassName("more")) {
                    x.style.display = 'block';
                }
            }
            else {
                x[0].innerHTML = "See more...";
                for (const x of document.getElementsByClassName("more")) {
                    x.style.display = 'none';
                }
            }
        }        
    }

    seeMore = () => {
        this.setState({
            ...this.state,
            see_more: !this.state.see_more
        });
    }

    showPaymentButton = () => {
        if (this.props.cart.length > 0) {
            return (
                <div className='payment'>
                    <div className='text'>
                        <h4>Total: </h4>
                        <h4 className='total'>${ (this.props.cartTotal).toFixed(2) }</h4>
                    </div>
                    <Link to="/checkout"><button type="button" className="btn btn-success">Make a payment</button></Link>
                </div>
            )
        }
    }

    onClickDelete = () => {
        if (this.props.cart.length > 0) {
            Swal.fire({
                title: 'Are you sure?',
                text: 'All items in your cart is going to be deleted!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, I am!'
            }).then((result) => {
                if (result.value) {
                    this.props.deleteCart();
                }
            });
        }
    }

    onDeleteOneItem = (itemID) => {
        this.props.deleteOneItemFromCart(itemID);
    }

    onChangeItemQuantity = (e, itemID) => {
        if (parseInt(e.target.value) <= 0 || e.target.value === null || e.target.value === '') {
            Swal.fire({
                type: 'error',
                title: 'Quantity must be greater than 0!',
                text: 'Please try again!'
            });
            return;
        }
        else {
            this.props.updateExistingItemQuantity(itemID, parseInt(e.target.value));
        }
    }

    handleInputFocus = (e) => {
        e.target.select();
    }

    render() {
        let us = localStorage.getItem('user');
        var showCategories = this.props.categories.map( (cate, index) => {
            return (
                <li key={cate.id} className={index < 3 ? "navLink" : "navLink more"} onClick={this.chooseCat}><Link to={`/categories/${cate.name}`}>{cate.name}</Link></li>
            )
        });

        var numberOfCartItems = 0;
        for (const iterator of this.props.cart) {
            numberOfCartItems += iterator.amount;
        }

        var usernameOrLogin = () => {
            if (this.props.isLoggedIn) {
                return(
                    <Fragment>
                        <li className='float-right' onClick={this.logOut}><Link to="/">Log out <i className="fas fa-sign-out-alt"></i></Link></li>
                        <li className='float-right'><Link to="/profile">{us} <i className="fas fa-address-card"></i></Link></li>
                    </Fragment>
                )
            } else {
                return (
                    <Fragment>
                        <li onClick={this.goToLoginOrSignupPage} className='float-right'><Link to="/signup">Signup <i className="fas fa-user-plus"></i></Link></li>
                        <li onClick={this.goToLoginOrSignupPage} className='float-right'><Link to="/login">Login <i className="fas fa-sign-in-alt"></i></Link></li>
                    </Fragment>
                )
            }
        }

        return (                        
            this.props.navBarIsShowed ?
            <div>
                <nav className="navbar navbar-default" role="navigation">
                            <div className="navbar-header">
                                <Link className="navbar-brand" to="/" onClick={this.getAllProducts}><i className="fab fa-glide-g"></i><b>Electronics</b></Link>
                            </div>
                            <div className="collapse navbar-collapse navbar-ex1-collapse">
                                <ul className="nav navbar-nav left">
                                    <li className="navLink" onClick={this.getAllProducts}><Link to="/">All Products</Link></li>
                                    {
                                        showCategories
                                    }
                                </ul>
                                <ul className="nav navbar-nav navbar-right">
                                    <li className='see-more'><Link to="">
                                        <label className='lbl' onClick={this.seeMore}>See more...</label>
                                    </Link></li>
                                    {
                                        usernameOrLogin()
                                    }
                                    <li className='float-right cart-icon'><Link to="/cart"><i className="fas fa-shopping-cart"></i></Link></li>
                                    <div className='mini-cart'>
                                        <div className="count-and-delete-all">
                                            <div className='count'>
                                                <h5>YOUR CART</h5>
                                                <p>({numberOfCartItems} items)</p>
                                            </div>
                                            <button type="button" className="btn btn-danger deleteAll" onClick={this.onClickDelete}>Delete all items</button>
                                        </div>
                                        <table className="table table-hover">
                                            <tbody>
                                                {
                                                    this.props.cart.length ?
                                                    this.props.cart.map( item => 
                                                        <tr key={item.product.id}>
                                                            <td className="img">
                                                                <img src={item.product.image ? item.product.image : "https://tuongtoanhhong.com/wp-content/themes/mytheme/public/images/empty-img.png"} alt=""/>
                                                            </td>
                                                            <th className="name-delete">
                                                                <div className="name">{item.product.name.length < 50 ? item.product.name : `${item.product.name.substring(0, 50)}...`}</div>
                                                                <div className="delete" onClick={() => this.onDeleteOneItem(item.product.id)}><i className="fas fa-trash-alt"></i> Delete from cart</div>
                                                            </th>
                                                            <td></td>
                                                            <th className="price">
                                                                ${ (item.product.price * item.amount).toFixed(2) }
                                                            </th>
                                                            <td className="quantity">
                                                                <input type="number" min="1" value={item.amount} onClick={this.handleInputFocus} onChange={(e) => this.onChangeItemQuantity(e, item.product.id)}/>
                                                            </td>
                                                        </tr>
                                                    ) :
                                                    <tr>
                                                        <td className='empty-cart'>
                                                            <img src="https://www.jivanayurveda.com/images/empty-cart.png" alt=""/>
                                                        </td>
                                                    </tr>
                                                }
                                            </tbody>
                                        </table>
                                        { this.showPaymentButton() }
                                    </div>
                                </ul>
                            </div>
                        </nav>
            </div> : 
            <div>
                <nav className="navbar navbar-default" role="navigation">
                    <div className="navbar-header">
                        <Link className="navbar-brand" to="/" onClick={this.getAllProducts}><i className="fab fa-glide-g"></i>Electronics</Link>
                    </div>
                </nav>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        products: state.productReducer.products,
        navBarIsShowed: state.navBarReducer.isShowed,
        isLoggedIn: state.navBarReducer.isLoggedIn,
        categories: state.categoryReducer.categories,
        cart: state.cartReducer.cart,
        cartTotal: state.cartReducer.total
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchAllProducts: (pageNumber) => {
            dispatch(productActions.actFetchProductsRequest(pageNumber));
        },
        fetchProductsByCategory: (cate, page) => {
            dispatch(productActions.fetchProductsByCategory(cate, page));
        },
        hideOrShowNavBar: (status) => {
            dispatch(navBarActions.hideOrShowNavBar(status));
        },
        setLoginStatus: (status) => {
            dispatch(navBarActions.setLoginStatus(status));
        },
        fetchAllCategories: () => {
            dispatch(cateActions.fetchAllCategories());
        },
        fetchCart: () => {
            dispatch(cartActions.fetchCart());
        },
        deleteOneItemFromCart: (itemID) => {
            dispatch(cartActions.deleteOneItem(itemID));
        },
        deleteCart: () => {
            dispatch(cartActions.deleteCart());
        },
        updateExistingItemQuantity: (itemID, amount, isAddition) => {
            dispatch(cartActions.updateCartItemQuantity(itemID, amount, isAddition));
        },
        checkout: () => {
            dispatch(cartActions.checkout());
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavigationBar)
