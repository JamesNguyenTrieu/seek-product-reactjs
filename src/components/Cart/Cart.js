import React, { Component } from 'react';
import './Cart.scss';
import * as actions from "./../../redux/actions/CartActions";
import { connect } from 'react-redux';
import Swal from 'sweetalert2';
import { Link, Redirect } from 'react-router-dom';

export class Cart extends Component {
    componentDidMount() {
        this.props.fetchCart();
    }

    onClickDelete = () => {
        if (this.props.cart.length > 0) {
            Swal.fire({
                title: 'Are you sure?',
                text: 'All items in your cart is going to be deleted!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, I am!'
            }).then((result) => {
                if (result.value) {
                    this.props.deleteCart();
                }
            });
        }
    }

    onDeleteOneItem = (itemID) => {
        this.props.deleteOneItemFromCart(itemID);
    }

    onChangeItemQuantity = (e, itemID) => {
        if (parseInt(e.target.value) <= 0 || e.target.value === null || e.target.value === '') {
            Swal.fire({
                type: 'error',
                title: 'Quantity must greater than 0!',
                text: 'Please try again!'
            });
            return;
        }
        else {
            this.props.updateExistingItemQuantity(itemID, parseInt(e.target.value));
        }
    }
    
    showPaymentButton = () => {
        if (this.props.cart.length > 0) {
            return (
                <div className='payment'>
                    <div className='text'>
                        <h3>Total: </h3>
                        <h3 className='total'>${ (this.props.total).toFixed(2) }</h3>
                    </div>
                    <Link to="/checkout"><button type="button" className="btn btn-success">Make a payment</button></Link>
                </div>
            )
        }        
    }

    render() {        
        const user = localStorage.getItem('user');
        if (user === null || user === undefined) {
            return <Redirect to="/login" />
        }
        let cartItemAmount = 0;
        for (const item of this.props.cart) {
            cartItemAmount += item.amount;
        }
        return (
            <div className="container">
                
                <div className="row">
                    <div className="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                    
                    <div className="col-xs-10 col-sm-10 col-md-10 col-lg-10 content">
                        
                        <div className="count-and-delete-all">
                            <div className='count'>
                                <h3>YOUR CART</h3>
                                <p>({cartItemAmount} items)</p>
                            </div>
                            <button type="button" className="btn btn-danger deleteAll" onClick={this.onClickDelete}>Delete all items</button>
                        </div>
                        <table className="table table-bordered table-hover">
                            <tbody>
                                {
                                    this.props.cart.length ?
                                    this.props.cart.map( (item, index)=> 
                                        <tr key={item.product.id}>
                                            <td className="img">
                                                <img src={item.product.image ? item.product.image : "https://tuongtoanhhong.com/wp-content/themes/mytheme/public/images/empty-img.png"} alt=""/>
                                            </td>
                                            <th className="name-delete">
                                                <div className="name">{item.product.name.length < 150 ? item.product.name : `${item.product.name.substring(0, 150)}...`}</div>
                                                <div className="delete" onClick={() => this.onDeleteOneItem(item.product.id)}><i className="fas fa-trash-alt"></i> Delete from cart</div>
                                            </th>
                                            <td></td>
                                            <th className="price">
                                                ${ (item.total).toFixed(2) }
                                            </th>
                                            <td className="quantity">
                                                <input type="number" defaultValue={item.amount} min="1" max={this.props.products && this.props.products[index] ? this.props.products[index].data.product.instock : "" } onChange={(e) => this.onChangeItemQuantity(e, item.product.id)}/>
                                            </td>
                                        </tr>
                                    ) :
                                    <tr>
                                        <td>
                                            <img src="https://www.jivanayurveda.com/images/empty-cart.png" alt=""/>
                                        </td>
                                    </tr>
                                }
                            </tbody>
                        </table>
                        { this.showPaymentButton() }
                    </div>

                    <div className="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                    
                </div>
                
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        cart: state.cartReducer.cart,
        total: state.cartReducer.total,
        products: state.cartReducer.products
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCart: () => {
            dispatch(actions.fetchCart());
        },
        deleteCart: () => {
            dispatch(actions.deleteCart());
        },
        deleteOneItemFromCart: (productID) => {
            dispatch(actions.deleteOneItem(productID));
        },
        updateExistingItemQuantity: (itemID, amount) => {
            dispatch(actions.updateCartItemQuantity(itemID, amount));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
