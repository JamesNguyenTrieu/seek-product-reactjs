import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import * as productActions from "../../redux/actions/ProductActions";
import * as cartActions from "./../../redux/actions/CartActions";
import * as navBarActions from "./../../redux/actions/NavActions";
import "./Product.scss";
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';
import Pagination from "react-js-pagination";

export class Product extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            categoryName: '',
            activePage: 1,
        }
        // this.props.fetchCart();
    }

    componentDidMount() {
        let categories = JSON.parse(localStorage.getItem('categories'));
        let URLPageNumber = this.props.match.params.pageNumber;
        let URLCategoryName = this.props.match.params.categoryName;
        if (URLPageNumber && URLPageNumber !== undefined && URLPageNumber !== "") {
            this.setState({
                activePage: URLPageNumber
            });
            this.props.fetchAllProducts(URLPageNumber);
        }
        else if (URLCategoryName && URLCategoryName !== undefined && URLCategoryName !== "") {
            let existed = false;
            for (const item of categories) {
                if (item.name === URLCategoryName) {
                    existed = true;
                    break;
                }
            }
            if (existed) {
                let page = this.props.match.params.categoryPage;
                if (page !== undefined && parseInt(page) !== undefined && parseInt(page) > 0) {
                    this.setState({
                        ...this.state,
                        activePage: page,
                        categoryName: this.props.match.params.CategoryName
                    });
                    this.props.fetchProductsByCategory(this.props.match.params.categoryName, page);
                }
                else {
                    this.setState({
                        ...this.state,
                        categoryName: this.props.match.params.CategoryName
                    });
                    this.props.fetchProductsByCategory(this.props.match.params.categoryName, 1);
                }
            } else {
                this.props.history.push('/notfound');
            }
        }
        else {
            this.props.fetchAllProducts(1);
        }
    }

    addToCart = (pid) => {
        this.props.propAddToCart(pid, 1);
        this.cartNotification('Added to cart successfully!')
    }

    cartNotification = (message) => {
        Swal.fire({
            position: 'top',
            type: 'success',
            title: message,
            showConfirmButton: false,
            timer: 1000
        });
    }

    userHasNotLoggedIn = () => {
        this.props.hideOrShowNavBar(false);
    }

    handlePageChange = (pageNumber) => {
        this.setState({
            ...this.state,
            activePage: pageNumber
        });
        let URLPageNumber = this.props.match.params.pageNumber;
        let URLCategoryName = this.props.match.params.categoryName;
        if (URLCategoryName && URLPageNumber === undefined) {
            this.props.history.push(`/categories/${this.props.match.params.categoryName}/${pageNumber}`);
            this.props.fetchProductsByCategory(URLCategoryName, pageNumber);
        }
        else {
            this.props.history.push(`/products/list/${pageNumber}`);
            this.props.fetchAllProducts(pageNumber);
        }
    }

    render() {        
        var goToCartOrLogin = (productID) => {
            if (localStorage.getItem('user')) {
                return(
                    <button onClick={() => this.addToCart(productID)} type="button" className="btn btn-warning add-to-cart-button">Add to Cart</button>
                )
            }
            else{
                return (
                    <Link to="/login" onClick={this.userHasNotLoggedIn}><button type="button" className="btn btn-warning add-to-cart-button">Add to Cart</button></Link>
                )
            }
        }
        var result = this.props.products.map((product, index) => {
            return (
                <div className="item" key={index}>
                    <Link to={`/products/details/${product.id}`}>
                        <img src={product.image ? product.image : "https://tuongtoanhhong.com/wp-content/themes/mytheme/public/images/empty-img.png"} className="img-responsive" alt={product.name}/>
                        <h4 className="product-name">{product.name.length < 50 ? product.name : `${product.name.substring(0, 50)}...`}</h4>
                        <h4>
                            ${ (product.price).toFixed(2) }
                        </h4>
                    </Link>
                    <div className="add-to-cart">
                        {
                            goToCartOrLogin(product.id)
                        }
                    </div>
                </div>
            )
        });
        return (
            <div className="container">
                {
                    this.props.products.length ?
                    <Fragment>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 list">
                            {result}
                        </div>
                        <Pagination className='pagination'
                            activePage={this.state.activePage}
                            itemsCountPerPage={12}
                            totalItemsCount={this.props.numberOfProducts}
                            pageRangeDisplayed={5}
                            onChange={this.handlePageChange}
                        />
                    </Fragment> :
                    <div className="not-found">
                        <img src="https://www.iamqatar.qa/assets/images/no-products-found.png" className="img-responsive" alt="" />
                    </div>
                }                  
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        products: state.productReducer.products,
        numberOfProducts: state.productReducer.numberOfProducts,
        navBarIsShowed: state.navBarReducer.isShowed,
        isLoggedIn: state.navBarReducer.isLoggedIn
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchAllProducts: (pageNumber) => {
            dispatch(productActions.actFetchProductsRequest(pageNumber));
        },
        fetchProductsByCategory: (categoryName, page) => {
            dispatch(productActions.fetchProductsByCategory(categoryName, page));
        },
        fetchCart: () => {
            dispatch(cartActions.fetchCart());
        },
        propAddToCart: (product, amount) => {
            dispatch(cartActions.actAddToCart(product, amount))
        },
        updateExistingItemQuantity: (itemID, amount) => {
            dispatch(cartActions.updateCartItemQuantity(itemID, amount));
        },
        hideOrShowNavBar: (status) => {
            dispatch(navBarActions.hideOrShowNavBar(status));
        },
        setLoginStatus: (status) => {
            dispatch(navBarActions.setLoginStatus(status));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Product);
