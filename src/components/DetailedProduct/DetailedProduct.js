import React, { Component } from 'react';
import "./DetailedProduct.scss";
import * as productActions from "../../redux/actions/ProductActions";
import * as cartActions from "./../../redux/actions/CartActions";
import { connect } from 'react-redux';
import Swal from 'sweetalert2';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

export class DetailedProduct extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            productID: this.props.match.params.productID,
            quantity: 0
        }
    }
    
    componentDidMount() {
        this.props.fetchOneProduct(this.state.productID);
    }

    onQuantityChange = (e) => {
        if (e.target.value !== this.state.quantity) {
            if (e.target.value === null || e.target.value === "" || parseInt(e.target.value) < 1) {
                this.setState({
                    ...this.state,
                    quantity: 1
                });
            }
            else if (this.props.product.product) {
                if (parseInt(e.target.value) >= this.props.product.product.in_stock) {
                    this.setState({
                        ...this.state,
                        quantity: this.props.product.product.in_stock
                    });
                }
                else {
                    this.setState({
                        ...this.state,
                        quantity: parseInt(e.target.value)
                    });            
                }
            }            
        }
    }

    addToCart = () => {
        if (localStorage.getItem('access_token') === null || localStorage.getItem('access_token') === undefined) {
            this.props.history.push("/login");
        }
        else {
            this.props.propAddToCart(this.state.productID, this.state.quantity);
            Swal.fire({
                position: 'top',
                type: 'success',
                title: 'Added to cart successfully!',
                showConfirmButton: false,
                timer: 1000
            });
        }
    }

    changeImage = (url) => {
        if (url === null) {
            url = "https://tuongtoanhhong.com/wp-content/themes/mytheme/public/images/empty-img.png";
        }
        document.getElementById('avatar').src=url;
    }

    goToRecommendedProductPage = (productID) => {
        this.props.fetchOneProduct(productID);
        this.props.history.push(`/products/details/${productID}`);
    }
    
    render() {
        var productFeeds = () => {
            if (this.props.product.feeds) {
                return (
                    <div className="small">
                        {
                            this.props.product.feeds.length ?
                            this.props.product.feeds.map(feed => {
                                return (
                                    <div key={feed.id} className="item">
                                        <img onClick={() => this.changeImage(feed.url)} src={feed.url ? feed.url : "https://tuongtoanhhong.com/wp-content/themes/mytheme/public/images/empty-img.png"} className="img-responsive" alt="" />
                                    </div>
                                )
                            }):
                            null
                        }
                    </div>
                    // <OwlCarousel
                    //     className="owl-theme small"
                    //     margin={10}
                    //     nav = {true}
                    //     dots = {false}
                    //     center
                    // >
                    //     {
                    //         this.props.product.feeds.length ?
                    //         this.props.product.feeds.map(feed => {
                    //             return (
                    //                 <div key={feed.id} className="item">
                    //                     <img onClick={() => this.changeImage(feed.url)} src={feed.url ? feed.url : "https://tuongtoanhhong.com/wp-content/themes/mytheme/public/images/empty-img.png"} className="img-responsive" alt="" />
                    //                 </div>
                    //             )
                    //         }):
                    //         null
                    //     }
                    // </OwlCarousel>
                )
            }
        };
        var recommendation = () => {
            if (this.props.product.other_products) {
                return (
                    <OwlCarousel
                        className="owl-theme large"
                        margin={10}
                        nav = {false}
                        dots = {false}
                        autoplay
                        autoplayTimeout = {3000}
                        autoplayHoverPause
                        rewind
                    >
                        {
                            this.props.product.other_products.length ?
                            this.props.product.other_products.map(pro => {
                                return (
                                    <div key={pro.id} className="item" onClick={() => this.goToRecommendedProductPage(pro.id)}>
                                        <img src={pro.image ? pro.image : "https://tuongtoanhhong.com/wp-content/themes/mytheme/public/images/empty-img.png"} className="img-responsive" alt="" />
                                        <h5>{pro.name.length < 20 ? pro.name : `${pro.name.substring(0, 20)}...`}</h5>
                                        <h4>${pro.price}</h4>
                                    </div>
                                )
                            }):
                            null
                        }
                    </OwlCarousel>
                )
            }
        }
        var result = () => {
            if (this.props.product) {
                return (
                    <div className="row">
                        <div className="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        </div>

                        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4 image">
                            <img id="avatar" src={this.props.product.product && this.props.product.product.image ? this.props.product.product.image : "https://tuongtoanhhong.com/wp-content/themes/mytheme/public/images/empty-img.png"} className="img-responsive" alt="" />
                            {
                                productFeeds()
                            }
                        </div>
                        
                        <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 info">

                            <table>
                                <tbody>
                                    <tr>
                                        <th colSpan="2">
                                            <h3>{this.props.product.product ? this.props.product.product.name : ""}</h3>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th className="fixed">Price:</th>
                                        <th>
                                            <h4>${this.props.product.product ? this.props.product.product.price : ""}</h4>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th className="fixed">Select quantity:</th>
                                        <th>
                                            <input type="number" className="form-control" onChange={this.onQuantityChange} defaultValue="1" min="1" max={this.props.product.product ? this.props.product.product.in_stock : 100} />
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>
                                            <button type="button" onClick={this.addToCart} className={this.props.product.product ? "btn btn-danger" : "btn btn-danger disabled"}>{this.props.product.product ? "Add to cart" : "Out of stock"}</button>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>

                            <div className="recommended">
                                <h3>Related Products:</h3>
                                {
                                    recommendation()
                                }
                            </div>
                            
                        </div>
                        
                        <div className="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        </div>
                    </div>
                )
            }
        }
        return (
            <div className="container">
                {result()}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        product: state.productReducer.detailedProduct
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchOneProduct: (productID) => {
            dispatch(productActions.fetchOneProduct(productID));
        },
        propAddToCart: (product, amount) => {
            dispatch(cartActions.actAddToCart(product, amount))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailedProduct);
