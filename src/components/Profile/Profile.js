import React, { Component } from 'react';
import { GetProfile, UpdateProfile } from '../../apiCaller/Authorization';
import "./Profile.scss";
import Swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';

export class Profile extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            first_name: "",
            last_name: "",
            phone_number: "",
            work_at: "",
            job: "",
            avatar: "",
            country: "",
            city: "",
            message: ""
        }
    }

    componentDidMount() {
        GetProfile().then(res => {
            if (res) {
                this.setState({
                    first_name: res.data.first_name,
                    last_name: res.data.last_name,
                    phone_number: res.data.phone_number,
                    work_at: res.data.work_at,
                    job: res.data.job,
                    avatar: res.data.avatar,
                    country: res.data.country,
                    city: res.data.city,
                    message: res.data.message
                })
            }            
        })
    }

    onFirstNameChange = (e) => {
        this.setState({
            ...this.state,
            first_name: e.target.value
        });
    }

    onLastNameChange = (e) => {
        this.setState({
            ...this.state,
            last_name: e.target.value
        });
    }

    onPhoneNumberChange = (e) => {
        this.setState({
            ...this.state,
            phone_number: e.target.value
        });
    }

    onJobChange = (e) => {
        this.setState({
            ...this.state,
            job: e.target.value
        });
    }

    onCompanyChange = (e) => {
        this.setState({
            ...this.state,
            work_at: e.target.value
        });
    }

    onCityChange = (e) => {
        this.setState({
            ...this.state,
            city: e.target.value
        });
    }

    onCountryChange = (e) => {
        this.setState({
            ...this.state,
            country: e.target.value
        });
    }

    onQuoteChange = (e) => {
        this.setState({
            ...this.state,
            message: e.target.value
        });
    }

    saveChanges = () => {
        let body = {
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            phone_number: this.state.phone_number,
            work_at: this.state.work_at,
            job: this.state.job,
            avatar: this.state.avatar,
            country: this.state.country,
            city: this.state.city,
            message: this.state.message
        }
        UpdateProfile(body).then(res => {
            if (res) {
                if (res.status === 200) {
                    Swal.fire({
                        type: "success",
                        title: "Updated your profile successfully!",
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
                else {
                    console.log(res.err.response.data);
                    Swal.fire({
                        type: "error",
                        title: "Opps! Something went wrong.",
                        text: res.err.response.statusText,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            }
        });
    }
    
    render() {
        const token = localStorage.getItem('access_token');
        if (token === null || token === undefined) {
            return <Redirect to="/login" />
        }
        return (
            <div className="row">
                <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">                    
                </div>

                <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">

                    <div className="panel panel-primary frame">
                        <div className="panel-heading header">
                            <img src={this.state.avatar ? this.state.avatar : "http://rentzet.com/homerun/assets/images/no-avatar.png"} className="img-responsive" alt="" />
                        </div>
                        <div className="panel-body body">
                            <table className="table">
                                <tbody>
                                    <tr>
                                        <th>
                                            First name:
                                        </th>
                                        <td className="text">
                                            <input className="form-control" type="text" onChange={this.onFirstNameChange} value={this.state.first_name ? this.state.first_name : ""} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Last name:</th>
                                        <td className="text">
                                            <input className="form-control" type="text" onChange={this.onLastNameChange} value={this.state.last_name ? this.state.last_name : ""}/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Phone number:</th>
                                        <td className="text">
                                            <input className="form-control" type="text" onChange={this.onPhoneNumberChange} value={this.state.phone_number ? this.state.phone_number : ""}/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Job:</th>
                                        <td className="text">
                                            <input className="form-control" type="text" onChange={this.onJobChange} value={this.state.job ? this.state.job : ""}/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Company:</th>
                                        <td className="text">
                                            <input className="form-control" type="text" onChange={this.onCompanyChange} value={this.state.work_at ? this.state.work_at : ""}/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>City:</th>
                                        <td className="text">
                                            <input className="form-control" type="text" onChange={this.onCityChange} value={this.state.city ? this.state.city : ""}/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Country:</th>
                                        <td className="text">
                                            <input className="form-control" type="text" onChange={this.onCountryChange} value={this.state.country ? this.state.country : ""}/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Quote:</th>
                                        <td className="text">
                                            <textarea cols="43" rows="3" className="form-control" type="text" onChange={this.onQuoteChange}  value={this.state.message ? this.state.message : ""}/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>

                            <button type="button" onClick={this.saveChanges} className="btn btn-success">Save changes</button>

                        </div>
                    </div>
                </div>

                <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                </div>
            </div>
        )
    }
}

export default Profile
