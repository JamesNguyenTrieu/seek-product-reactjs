import React, { Component } from 'react';
import {Elements, StripeProvider} from 'react-stripe-elements';
import CheckOut from './CheckOut';

export class Payment extends Component {
    render() {
        return (
            <StripeProvider apiKey="pk_test_7X4at47jVmUqka7N8HhdO35N">               
                
                <div className="row">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        
                    </div>
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div className="panel panel-primary">
                            <div className="panel-heading">
                                    <h3>Payment Information</h3>
                            </div>
                            <div className="panel-body">
                                    <Elements>
                                        <CheckOut />
                                    </Elements>
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        
                    </div>
                </div>
            </StripeProvider>
        )
    }
}

export default Payment
