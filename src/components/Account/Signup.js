import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import { connect } from 'react-redux';
import * as actions from "./../../redux/actions/NavActions";
import { CallSignup, CallLogin } from "./../../apiCaller/Authorization";

export class Signup extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             firstName: '',
             lastName: '',
             email: '',
             password: '',
             confirmedPassword: ''
        }
    }

    componentDidMount() {
        this.checkURL();
    }
    
    checkURL = () => {
        let navBarShowed = true;
        let currentPage = window.location.href;
        currentPage = (currentPage).slice(currentPage.lastIndexOf("/")+1, currentPage.length);
        if (currentPage === 'signup' || currentPage === 'login') {
            navBarShowed = false;
        }

        this.props.hideOrShowNavBar(navBarShowed);
    }

    labelStyle = {
        float: "left"
    }

    onFirstNameChange = (e) => {
        this.setState({
            ...this.state,
            firstName: e.target.value
        });
    }

    onLastNameChange = (e) => {
        this.setState({
            ...this.state,
            lastName: e.target.value
        });
    }

    onEmailChange = (e) => {
        this.setState({
            ...this.state,
            email: e.target.value
        });
    }

    onPasswordChange = (e) => {
        this.setState({
            ...this.state,
            password: e.target.value
        });
    }

    onPasswordConfirming = (e) => {
        this.setState({
            ...this.state,
            confirmedPassword: e.target.value
        });
    }

    onSubmit = (e) => {
        e.preventDefault();

        CallSignup(this.state.firstName, this.state.lastName, this.state.email, this.state.password, this.state.confirmedPassword)
        .then(suc => {
            if (suc !== undefined) {
                CallLogin(suc.data.email, this.state.password)
                .then(res => {
                    Swal.fire({
                        position: 'top',
                            type: 'success',
                            title: 'Signed up successfully!',
                            showConfirmButton: false,
                            timer: 1000
                    });
                    localStorage.setItem('user', suc.data.email);
                    localStorage.setItem('access_token', res.data.token);
                    this.props.hideOrShowNavBar(true);
                    this.props.history.push("/");
                });
            }
        }, err => {
            let mes = JSON.parse(err.response.request.response);
            if (mes.email) {
                Swal.fire({
                    position: 'top',
                    type: 'error',
                    title: 'Invalid email!',
                    text: `${mes.email[0]}! Please try again.`
                });
            }
            else if (mes.password) {
                Swal.fire({
                    position: 'top',
                    type: 'error',
                    title: 'Invalid password!',
                    text: `${mes.password[0]}! Please try again.`
                });
            }
            else if (mes.confirm_password) {
                Swal.fire({
                    position: 'top',
                    type: 'error',
                    title: 'Invalid confirmed password!',
                    text: `${mes.confirm_password[0]}! Please try again.`
                });
            }
        });
    }

    isUserNameExisted = (userArr, us, pwd) => {
        for (const user of userArr) {
            if (user.username === us && user.password === pwd)
                return true;
        }
        return false;
    }

    render() {
        var user = localStorage.getItem("user");
        if (user !== null) {
            return <Redirect to="/" />
        }
        return (
            <div className="container">
                
                <div className="row">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        
                    </div>
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        
                        <div className="panel panel-warning">
                              <div className="panel-heading">
                                    <legend className="panel-title">
                                        <h3>Signup</h3>
                                    </legend>
                              </div>
                              <div className="panel-body">                                
                                    <form onSubmit={this.onSubmit}>
                                        <div className="form-group">
                                            <label style={this.labelStyle}>First Name:</label>
                                            <input type="text" className="form-control" onChange={this.onFirstNameChange}/>
                                        </div>
                                        <div className="form-group">
                                            <label style={this.labelStyle}>Last Name:</label>
                                            <input type="text" className="form-control" onChange={this.onLastNameChange}/>
                                        </div>
                                        <div className="form-group">
                                            <label style={this.labelStyle}>Email:</label>
                                            <input type="email" className="form-control" onChange={this.onEmailChange}/>
                                        </div>
                                        <div className="form-group">
                                            <label style={this.labelStyle}>Password:</label>
                                            <input type="password" className="form-control" onChange={this.onPasswordChange}/>
                                        </div>
                                        <div className="form-group">
                                            <label style={this.labelStyle}>Confirmed password:</label>
                                            <input type="password" className="form-control" onChange={this.onPasswordConfirming}/>
                                        </div>

                                        <button type="submit" className="btn btn-success">Create my account</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button type="reset" className="btn btn-danger">Reset</button>
                                    </form>
                                    <br></br>
                                    <Link to='/login'>You've had an account? Click here to log in!</Link>
                              </div>
                        </div>
                        
                    </div>
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        
                    </div>
                </div>
                
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        isShowed: state.navBarReducer.isShowed
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        hideOrShowNavBar: (status) => {
            dispatch(actions.hideOrShowNavBar(status));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
