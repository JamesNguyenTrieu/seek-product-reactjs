import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import * as navActions from "./../../redux/actions/NavActions";
import * as cartActions from "./../../redux/actions/CartActions";
import { connect } from 'react-redux';
import { CallLogin } from "./../../apiCaller/Authorization";
import Axios from 'axios';
import { API_URL } from '../../apiCaller/config';

export class Login extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             email: '',
             password: ''
        }
    }

    componentDidMount() {
        this.checkURL();
    }

    checkURL = () => {
        let navBarShowed = true;
        let currentPage = window.location.href;
        currentPage = (currentPage).slice(currentPage.lastIndexOf("/")+1, currentPage.length);
        if (currentPage === 'signup' || currentPage === 'login') {
            navBarShowed = false;
        }

        this.props.hideOrShowNavBar(navBarShowed);
    }

    getEmail = (e) => {
        this.setState({
            ...this.state,
            email: e.target.value
        });
    }

    getPassword = (e) => {
        this.setState({
            ...this.state,
            password: e.target.value
        });
    }
    
    onLogIn = (e) => {
        e.preventDefault();
        CallLogin(this.state.email, this.state.password)
        .then(res => {
            if (res !== undefined) {
                Swal.fire({
                    position: 'top',
                    type: 'success',
                    title: 'Logged in successfully!',
                    showConfirmButton: false,
                    timer: 1000
                });
                localStorage.setItem('user', this.state.email);
                localStorage.setItem('access_token', res.data.token);                
                this.props.hideOrShowNavBar(true);
                this.props.history.push("/");
                this.props.fetchCart();
            }
        }, err => {
            let mes = JSON.parse(err.response.request.response);
            if (mes.email) {
                Swal.fire({
                    position: 'top',
                    type: 'error',
                    title: 'Wrong credentials!',
                    text: `${mes.email[0]}! Please try again.`
                });
            }
            else if (mes.password) {
                Swal.fire({
                    position: 'top',
                    type: 'error',
                    title: 'Invalid password!',
                    text: `${mes.password[0]}! Please try again.`
                });
            }
        });
    }

    render() {        
        var user = localStorage.getItem("user");
        if (user !== null) {
            return <Redirect to="/" />
        }
        return (
            <div className="container">
                
                <div className="row">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        
                    </div>
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        
                        <div className="panel panel-warning">
                              <div className="panel-heading">
                                    <legend className="panel-title">
                                        <h3>Login</h3>
                                    </legend>
                              </div>
                              <div className="panel-body">
                                    
                                    <form onSubmit={this.onLogIn}>                                    
                                        <div className="form-group">
                                            <input type="email" className="form-control" placeholder="Email" required onChange={this.getEmail}/>
                                        </div>
                                        <div className="form-group">
                                            <input type="password" className="form-control" placeholder="Password" required onChange={this.getPassword}/>
                                        </div>

                                        <button type="submit" className="btn btn-success">Log in</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button type="reset" className="btn btn-danger">Reset</button>
                                    </form>
                                    <br></br>
                                    <Link to="/signup">You not have an account? Sign up here!</Link>
                              </div>
                        </div>
                        
                    </div>
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        
                    </div>
                </div>
                
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        isShowed: state.navBarReducer.isShowed,
        cart: state.cartReducer.cart
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        hideOrShowNavBar: (status) => {
            dispatch(navActions.hideOrShowNavBar(status));
        },
        fetchCart: () => {
            dispatch(cartActions.fetchCart());
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);