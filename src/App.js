import React from 'react';
import { Provider } from "react-redux";
import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import store from "./redux/stores/CombinedStore";
import Product from "./components/Product/Product";
import NotFound from "./components/NotFound/NotFound";
import NavigationBar from './components/NavBar/NavigationBar';
import Login from './components/Account/Login';
import Signup from './components/Account/Signup';
import Cart from './components/Cart/Cart';
import Payment from './components/CheckOut/Payment';
import DetailedProduct from './components/DetailedProduct/DetailedProduct';
import Profile from './components/Profile/Profile';

// const MenuLink = ({label, to, activeOnlyWhenExact}) => {
//   return(
//     <Route path={to} exact={activeOnlyWhenExact} children={({ match }) => {
//       var active = match ? 'active' : '';
//       return (
//         <li className={active}>
//           <Link to={to}>{label}</Link>
//         </li>
//       )
//     }}></Route>
//   )
// }

// const showMenuContent = (routes) => {
//   var result = null;
//   if (routes.length > 0) {
//     result = routes.map( (route, index) => {
//       return (
//         <Route
//           key={index}
//           path = {route.path}
//           exact = {route.exact}
//           component = {route.main}
//         />
//       )
//     } );
//   }
//   return result;
// }

function App() {  
  return (
    <Provider store={store}>
      <Router>
          <div className="App">
            <NavigationBar></NavigationBar>
            {/* Content */}
            <Switch>
              {/* {showMenuContent(routes)} */}
              <Route path="/" exact component={Product}/>
              <Route path="/products/list/:pageNumber" exact component={Product}/>
              <Route path="/categories/:categoryName" exact component={Product}/>
              <Route path="/categories/:categoryName/:categoryPage" exact component={Product}/>
              <Route path="/products/details/:productID" exact component={DetailedProduct}/>
              <Route path="/login" exact component={Login}/>
              <Route path="/signup" exact component={Signup}/>
              <Route path="/cart" exact component={Cart}/>
              <Route path="/checkout" exact component={Payment}/>
              <Route path="/profile" exact component={Profile}/>
              <Route path="/notfound" exact component={NotFound}/>
              <Route component={NotFound} />
            </Switch>
          </div>
        </Router>
    </Provider>
  );
}



export default App;
