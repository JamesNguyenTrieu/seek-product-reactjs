import callAPI, { API_URL } from "./config";
import { headers } from './CartCaller'
import callAPIWithHeaders from "./configWithHeaders";
import Axios from "axios";

export const CallLogin = (email, password) => {
    let body = {
        email,
        password
    };
    return Axios.post(`${API_URL}/user/login/`, body, null);
}

export const CallSignup = (first_name, last_name, email, password, confirm_password) => {
    let body = {
        first_name,
        last_name,
        email,
        password,
        confirm_password
    };
    return Axios.post(`${API_URL}/api/auth/register/`, body, null);
}

export const GetProfile = () => {
    return callAPIWithHeaders(`api/auth/profile`, 'GET', null, headers(localStorage.getItem('access_token')));
}

export const UpdateProfile = (body) => {
    return callAPIWithHeaders(`api/auth/profile`, 'PUT', body, headers(localStorage.getItem('access_token')));
}