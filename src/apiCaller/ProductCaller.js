import callAPI from "./config";

export const GetProducts = (page) => {
    return callAPI(`api/products/list?page=${page}&page_size=${12}`);
}

export const GetCategoryProducts = (cate, page) => {
    var body = {
        "key_word": "",
        "category": cate
    }
    return callAPI(`api/category/search/?order_by=-id&page=${page}&page_size=12`, 'POST', body);
}

export const GetDetailedProduct = (productID) => {
    return callAPI(`api/products/${productID}/details`);
}