import callAPIWithHeaders from "./configWithHeaders";
import Axios from "axios";
import callAPI, { API_URL } from "./config";

export const headers = (token) => {
    return {
        "Content-Type": "application/json",
        "Authorization": `JWT ${token}`
    }
};

export const GetCart = () => {
    return callAPIWithHeaders('api/user/cart/view-cart/', 'GET', null, headers(localStorage.getItem('access_token')));
}

export const AddToCartCaller = (product, amount) => {
    let body = {
        product,
        amount
    }
    return callAPIWithHeaders('api/user/cart/add-to-cart', 'POST', body, headers(localStorage.getItem('access_token')));
}

export const RemoveFromCart = (productID) => {
    let body = {
        "product": productID
    }
    return callAPIWithHeaders('api/user/cart/remove-from-cart', 'POST', body, headers(localStorage.getItem('access_token')));
}

export const DeleteAllCartItems = () => {
    return callAPIWithHeaders('api/user/cart/clear', 'GET', null, headers(localStorage.getItem('access_token')));
}

export const UpdateCart = (product, amount) => {
    let body = {
        product,
        amount
    }
    return callAPIWithHeaders('api/user/cart/update-cart', 'POST', body, headers(localStorage.getItem('access_token')));
}

export const CartPayment = (name, address_line1, token) => {
    let body = {
        option: 2,
        name,
        address_line1,
        token: token
    }
    return Axios({
        method: 'POST',
        url: `${API_URL}/api/user/cart/payment`, 
        data: body, 
        headers: headers(localStorage.getItem('access_token'))
    });
}

export const CartPayment2 = (name, address_line1, token) => {
    let body = {
        option: 2,
        name,
        address_line1,
        token: token
    }
    return callAPIWithHeaders('/api/user/cart/payment', 'POST', body, headers(localStorage.getItem('access_token')));
}