import axios from "axios";
export const API_URL = 'http://api.seekproduct.com';

export default function callAPI(endpoint, method = 'GET', body) {
    return axios({
        method: method,
        url: `${API_URL}/${endpoint}`,
        data: body
    }).catch(err => {
        console.log(err);     
    })
}