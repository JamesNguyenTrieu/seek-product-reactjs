import axios from "axios";
export const API_URL = 'http://api.seekproduct.com';

export default function callAPIWithHeaders(endpoint, method = 'GET', body, headers) {
    return axios({
        method: method,
        url: `${API_URL}/${endpoint}`,
        data: body,
        headers
    }).catch(err => {
        console.log({err});
    })
}